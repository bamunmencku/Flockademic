global.fetch = require('jest-fetch-mock');

// Polyfill for requestAnimationFrame, which is used by React since v16:
// https://reactjs.org/docs/javascript-environment-requirements.html
require('raf/polyfill');

var Enzyme = require('enzyme');
var Adapter = require('enzyme-adapter-react-16');
Enzyme.configure({ adapter: new Adapter() });

// Make sure Luxon formats dates in a consistent time zone, regardless of the developer's location:
var Luxon = require('luxon');
Luxon.Settings.defaultZoneName = 'utc';