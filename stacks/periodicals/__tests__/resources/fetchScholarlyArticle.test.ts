jest.mock('../../src/services/scholarlyArticle', () => ({
  fetchScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve({
    author: [ {
      identifier: 'Arbitrary account id',
    } ],
    identifier: 'arbitrary_identifier',
  })),
}));

import { fetchScholarlyArticle } from '../../src/resources/fetchScholarlyArticle';

const mockContext = {
  database: {} as any,
  session: { identifier: 'Arbitrary ID' },

  body: undefined,
  headers: {},
  method: 'GET' as 'GET',
  params: [ '/articles/arbitrary_id/', 'arbitrary_id' ],
  path: '/articles/arbitrary_id/',
  query: null,
};

it('should error when no article ID was specified', () => {
  const promise = fetchScholarlyArticle({ ...mockContext, params: [], path: '/articles/' });

  return expect(promise).rejects.toEqual(new Error('Could not find an article without an article ID.'));
});

it('should error when there was an error accessing the database', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const promise = fetchScholarlyArticle({
    ...mockContext,
    params: [ '/articles/some_id', 'some_id' ],
    path: '/articles/some_id',
  });

  return expect(promise).rejects.toEqual(new Error('Could not find an article with ID `some_id`.'));
});

it('should return an empty object when the given article could not be found', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.resolve(null));

  const promise = fetchScholarlyArticle({
    ...mockContext,
    params: [ '/articles/arbitrary_id', 'arbitrary_id' ],
    path: '/articles/arbitrary_id',
  });

  return expect(promise).resolves.toEqual({});
});

it('should call the command handler when all parameters are correct', (done) => {
  const mockedCommandHandler = require.requireMock('../../src/services/scholarlyArticle');

  const promise = fetchScholarlyArticle({
    ...mockContext,
    params: [ '/articles/some_id', 'some_id' ],
    path: '/articles/some_id',
    session: { identifier: 'Arbitrary creator' },
  });

  setImmediate(() => {
    expect(mockedCommandHandler.fetchScholarlyArticle.mock.calls.length).toBe(1);
    expect(mockedCommandHandler.fetchScholarlyArticle.mock.calls[0][1]).toBe('some_id');
    done();
  });
});

// tslint:disable-next-line:max-line-length
it('should not pass an error to the fetchScholarlyArticle service if the user\'s session could not be found', (done) => {
  const mockedCommandHandler = require.requireMock('../../src/services/scholarlyArticle');

  const promise = fetchScholarlyArticle({
    ...mockContext,
    session: new Error('No session found'),
  });

  setImmediate(() => {
    expect(mockedCommandHandler.fetchScholarlyArticle.mock.calls.length).toBe(1);
    expect(mockedCommandHandler.fetchScholarlyArticle.mock.calls[0][2]).toBeUndefined();
    done();
  });
});
